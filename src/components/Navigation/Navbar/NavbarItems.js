import React from 'react';
import NavigationItem from './NavbarItem/NavbarItem';
import classes from './NavbarItems.module.scss';

const Nav = () => (
    <ul className={classes.NavigationItems}>
        <NavigationItem link="/" active>Home</NavigationItem>
        <NavigationItem link="/about">About</NavigationItem>
        <NavigationItem link="/contactUs">Contact Us</NavigationItem>
    </ul>
);

export default Nav;
