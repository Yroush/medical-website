import React from 'react';
import Nav from './components/Navigation/Navbar/NavbarItems';
import Home from './Containers/HomePage/Home';
import About from './Containers/About/About';
import ContactUs from './Containers/Contact Us/ContactUs';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
function App() {
  return (
    <Router>
      <div className="App">
        <Nav />
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/about" component={About} />
          <Route path="/contactUs" component={ContactUs} />
        </Switch>
      </div>
    </Router>
  );
}

export default App;
